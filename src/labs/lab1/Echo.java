package labs.lab1;
//Mario Cerecer
//CS201
//Section 1
//01-24-2021
//Echo
import java.util.Scanner;

public class Echo {

	public static void main(String[] args) {
	Scanner input = new Scanner(System.in);
	
	System.out.print("Enter your name ");
	String name = input.nextLine();
	
	input.close();
	
	System.out.print(name);
}
}