package labs.lab1;
//Mario Cerecer
//CS201
//Section 1
//01-24-2021
//Box
import java.util.Scanner;

public class Box {
	public static void main(String[] args) {
		//Test Table
		//Input (length-width-depth)		/		Expected Result		/		Given Result
		//13-20-2							/		4.52778				/		4.52777
		//50-73-80							/		187.36111			/		187.36099
		//4-2-9								/		0.861111			/		0.86111056
		//50-92-143							/		345.91667			/		345.91644
		Scanner input = new Scanner (System.in);
		
		System.out.print("Enter the lenght in inches ");
		int lenght= Integer.parseInt(input.nextLine());
		
		System.out.print("Enter the width in inches ");
		int width= Integer.parseInt(input.nextLine());
		
		System.out.print("Enter the depth in inches ");
		int depth= Integer.parseInt(input.next());
		
		input.close();
		
		int si= 2*((width*lenght)+(depth*lenght)+(depth*width));
		double sf=(si*0.00694444);
		
		System.out.println("The amout of wood (in square feet) needed is " +(sf)) ;
		
		
	}
}
