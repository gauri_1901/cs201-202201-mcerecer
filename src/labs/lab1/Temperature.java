package labs.lab1;
//Mario Cerecer
//CS201
//Section 1
//01-24-2021
//Temperatures
import java.util.Scanner;

public class Temperature {
	public static void main(String[] args) {
		//Test Table (fahrenheit to celcius)
		//Input			/		Expected Result		/	Given Result
		//	10					-12.22					-12.22222
		//	23					-5						-5.0
		//	35					1.66667					1.666666
		//	57					13.8889					13.88888
		//	72					22.2222					22.2222
		//Test Table (celcius to fahrenheit)
		//Input			/		Expected Result		/	Given Result
		//	10					50						50.0
		//	23					73.4					73.4
		//	35					95						95.0
		//	57					134.6					134.6
		//	72					161.6					161.6
		
		//My program works as expected
		Scanner input = new Scanner (System.in);
		
		System.out.print("Enter a temperature (in Fahrenheit) ");
		double fahren= Double.parseDouble(input.next());
				
				System.out.print("Enter a temperature (in Celcius) ");
				double celcius= Double.parseDouble(input.next());
				
				
				double fc= (fahren-32);
				double fc2=((5*fc)/9);
		System.out.println("Fahrenheit to celcius is " +fc2);
		
		double cf=((9*celcius)/5);
		double cf2=cf+32;
		
		System.out.println("Celcius to Fahrenheit is " +cf2);
	}

}
