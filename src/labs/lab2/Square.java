package labs.lab2;
//Mario Cerecer
//CS201
//Section 1
//01-31-2021
//Square
//This programs prompts the user for the dimensions of a cube and create a cube with dimensions entered
import java.util.Scanner;

public class Square {

	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);//open scanner
		
		System.out.print("Enter a size ");//Ask for size
		
		int size = Integer.parseInt(input.nextLine());// Enter size
		
		input.close();//close scanner
		
		for (int x=1; x <= size; x++) {//sides of square
				for(int y=1; y <= size; y++){//top and bottom
						System.out.print("*");
				}
				System.out.println();
		}
	}
}
